export interface CourseInterface {
	id: number;
	title: string;
	date: Date;
	duration: number;
	description: string;
	topRated: boolean;
}
